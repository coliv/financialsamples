# README #

C++ classes from the book 

Practical C++ Financial Programming 
by Carlos Oliveira.
Apress, 2015

http://amzn.to/1C8ekwg 

### Summary ###

You will find a set of useful classes for financial programming projects, as explained in the book.

### How do I get set up? ###

The code has been tested on Windows and Mac OS X platforms. Only standard C++ features are used, so you should be able to compile these classes using any standards-compliant C++ compiler.


